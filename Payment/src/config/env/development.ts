'use strict';

module.exports = {
	app: {
		title: 'Payment',
		description: 'Payment',
		url: 'http://localhost:8087'
	},
	port: process.env.NODEJS_PORT || 8087,
	hostname: process.env.NODEJS_IP || 'localhost',
	authorization: 'mysecrettoken',
	db: {
		mssql: {
			root: {
				user: '',
				password: '',
				server: '',
				database: '',
				options: {
					trustedConnection: false
				}
			}
		},
		geode: {
			host: 'http://localhost:8111',
			dbinfo: {
				myregion: '/myregion'
			}
		},
		sequelize: {
			mydb: {
				dialect: "mssql",
				host: "",
				username: "",
				password: "",
				database: "",
				dialectOptions: {
					port: 1433
				}
			}
		}
	},
	ai: {
		wit: {
			accessToken: ""
		}
	}
};
