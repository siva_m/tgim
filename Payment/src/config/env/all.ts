'use strict';

module.exports = {
	app: {
		title: 'Payment',
		description: 'Payment',
		url : 'http://localhost'
	},
  port: process.env.NODEJS_PORT || 8081,
	hostname: process.env.NODEJS_IP || 'localhost'
};
