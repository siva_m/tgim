'use strict';


module.exports = function(app) {
  // var authCtrl = require('../../app/controllers/auth.server.controller');

  // app.route('/api').post(authCtrl.authenticate, apiCtrl.loadDataToGeode);

  // Finish with setting up the companyId param
  //app.param('Id', apiCtrl.func);
  var items=require('../../app/controllers/api.server.controller');

  app.route('/items').get(items.displayItems);
  app.route('/items').post(items.insertItems);
  app.route('/items').put(items.updateItems);
  app.route('/items').delete(items.deleteItems);

};
