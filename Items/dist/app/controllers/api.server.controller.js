var Client = require('mariasql');
var c = new Client({
    host: 'localhost',
    user: 'root',
    password: 'root'
});
exports.insertItems = function (req, res) {
    c.query('use ramcoeats', function (err) {
        if (err)
            throw err;
        var items = req.body;
        c.query('insert into items values(:itemid,:itemname,:price,:timing,:Availability)', { itemid: items.item_id, itemname: items.item_name, price: items.price, timing: items.timing, Availability: items.Availability }, function (err, rows) {
            if (err)
                throw err;
            res.send(rows);
        });
        c.end();
    });
};
exports.updateItems = function (req, res) {
    c.query('use ramcoeats', function (err) {
        if (err)
            throw err;
        var items = req.body;
        c.query('update items set  item_name=:itemname, price=:price, timing=:timing, Availability=:Availability where item_id=:itemid', { itemid: items.item_id, itemname: items.item_name, price: items.price, timing: items.timing, Availability: items.Availability }, function (err, rows) {
            if (err)
                throw err;
            res.send(rows);
        });
        c.end();
    });
};
exports.deleteItems = function (req, res) {
    c.query('use ramcoeats', function (err) {
        if (err)
            throw err;
        var items = req.body;
        c.query('delete from items where item_id=:id ', { id: items.item_id }, function (err, rows) {
            if (err)
                throw err;
            res.send(rows);
        });
        c.end();
    });
};
exports.displayItems = function (req, res) {
    c.query('use ramcoeats', function (err) {
        if (err)
            throw err;
        c.query('select * from items', function (err, rows) {
            if (err)
                throw err;
            res.send(rows);
        });
        c.end();
    });
};
//# sourceMappingURL=api.server.controller.js.map