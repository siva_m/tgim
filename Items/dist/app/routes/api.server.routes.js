'use strict';
module.exports = function (app) {
    var items = require('../../app/controllers/api.server.controller');
    app.route('/items').get(items.displayItems);
    app.route('/items').post(items.insertItems);
    app.route('/items').put(items.updateItems);
    app.route('/items').delete(items.deleteItems);
};
//# sourceMappingURL=api.server.routes.js.map