'use strict';
module.exports = {
    app: {
        title: 'Items',
        description: 'Items',
        url: 'http://localhost:8086'
    },
    port: process.env.NODEJS_PORT || 8086,
    hostname: process.env.NODEJS_IP || 'localhost',
    authorization: 'mysecrettoken',
    db: {
        mssql: {
            root: {
                user: '',
                password: '',
                server: '',
                database: '',
                options: {
                    trustedConnection: false
                }
            }
        },
        geode: {
            host: 'http://localhost:8111',
            dbinfo: {
                myregion: '/myregion'
            }
        },
        sequelize: {
            mydb: {
                dialect: "mssql",
                host: "",
                username: "",
                password: "",
                database: "",
                dialectOptions: {
                    port: 1433
                }
            }
        }
    },
    ai: {
        wit: {
            accessToken: ""
        }
    }
};
//# sourceMappingURL=development.js.map