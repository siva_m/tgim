'use strict';

module.exports = {
	app: {
		title: 'Order',
		description: 'Order items',
		url: ''
	},
	port: process.env.NODEJS_PORT || 8088,
	hostname: process.env.NODEJS_IP || 'localhost',
	authorization: 'mysecrettoken',
	db: {
		mssql: {
			root: {
				user: '',
				password: '',
				server: '',
				database: '',
				options: {
					trustedConnection: false
				}
			}
		},
	},
	ai: {
		wit: {
			accessToken: ""
		}
	}
};
