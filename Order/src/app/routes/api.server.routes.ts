'use strict';


module.exports = function(app) {
  // var authCtrl = require('../../app/controllers/auth.server.controller');

  // app.route('/api').post(authCtrl.authenticate, apiCtrl.loadDataToGeode);

  // Finish with setting up the companyId param
  //app.param('Id', apiCtrl.func);

  var orders=require('../../app/controllers/api.server.controller');

  app.route('/orders').get(orders.displayOrders);
  app.route('/orders/active').get(orders.displayActiveOrders);
  app.route('/orders').post(orders.insertOrder);
  app.route('/orders/:orderid').get(orders.displayOrder);
  app.route('/orders').put(orders.deleteOrder);

//  app.route('/orders/addtocart/:employeeid').get(orders.x);

};
