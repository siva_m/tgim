var Client = require('mariasql');
var barcode = require('jsbarcode');
var Canvas = require("canvas");
var c = new Client({
    host: 'localhost',
    user: 'root',
    password: 'root'
});
var order_utils = require('../../app/utils/order.utils');
function addToCart(req, res) {
    return new Promise(function (resolve, reject) {
        order_utils.createTable()
            .then(order_utils.generateOrderId)
            .then(function (orderid) { resolve(order_utils.insertAndRetrieve(req, res, orderid)); })
            .catch(err => reject(err));
    });
}
exports.insertOrder = function (req, res) {
    var items = req.body;
    c.query('use ramcoeats', function (err) {
        c.query('select Availability from items where item_id=:i_id', { i_id: items.item_id }, function (err, rows) {
            if (err)
                throw err;
            if (rows[0].Availability !== 'Out of stock') {
                addToCart(req, res).then(function (result) {
                    console.log(result);
                    c.query('use ramcoeats', function (err) {
                        c.query('insert into orders values(:orderid,:employeeid,:itemid,:quantity,:status)', { orderid: result, employeeid: items.employee_id, itemid: items.item_id, quantity: items.quantity, status: items.status }, function (err, rows) {
                            if (err) {
                                throw err;
                            }
                            var JsBarcode = require('jsbarcode');
                            var Canvas = require("canvas");
                            var canvas = new Canvas();
                            if (JsBarcode(canvas, result)) {
                                res.send('<img src="' + canvas.toDataURL() + '"/>');
                            }
                            else {
                                res.send('Error in creating barcode');
                            }
                        });
                    });
                    c.end();
                });
            }
            else {
                console.log('Items out-of-stock');
                res.send('Items out-of-stock');
            }
        });
    });
};
exports.deleteOrder = function (req, res) {
    c.query('use ramcoeats', function (err) {
        if (err)
            throw err;
        var items = req.body;
        c.query('update orders set status=\'cancelled\' where order_id=:id ', { id: items.order_id }, function (err, rows) {
            if (err)
                throw err;
            res.send(rows);
        });
        c.end();
    });
};
exports.displayOrders = function (req, res) {
    c.query('use ramcoeats', function (err) {
        if (err)
            throw err;
        c.query('select * from orders', function (err, rows) {
            if (err)
                throw err;
            res.send(rows);
        });
        c.end();
    });
};
exports.displayActiveOrders = function (req, res) {
    c.query('use ramcoeats', function (err) {
        if (err)
            throw err;
        c.query('select * from orders where status="active" ', function (err, rows) {
            if (err)
                throw err;
            res.send(rows);
        });
        c.end();
    });
};
exports.displayOrder = function (req, res) {
    c.query('use ramcoeats', function (err) {
        if (err)
            throw err;
        var order_id = req.params.orderid;
        c.query('select * from orders where order_id=:orderid', { orderid: order_id }, function (err, rows) {
            if (err)
                throw err;
            res.send(rows);
        });
        c.end();
    });
};
//# sourceMappingURL=api.server.controller.js.map