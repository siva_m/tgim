var Client = require('mariasql');
var c = new Client({
    host: 'localhost',
    user: 'root',
    password: 'root'
});
exports.createTable = function () {
    return new Promise(function (resolve, reject) {
        c.query('use ramcoeats', function (err) {
            if (err)
                reject(err);
            c.query('create table if not exists cart(orderid varchar(20), employeeid varchar(20))', function (err) {
                if (err)
                    reject(err);
                resolve('table created');
            });
        });
    });
};
exports.generateOrderId = function () {
    var crypto = require('crypto');
    function randomValueBase64(len) {
        return crypto
            .randomBytes(Math.ceil((len * 3) / 4))
            .toString('base64')
            .slice(0, len)
            .replace('+', '0')
            .replace('/', '0');
    }
    return new Promise(function (resolve, reject) {
        if (randomValueBase64(12)) {
            resolve(randomValueBase64(12));
        }
        else {
            reject('Order ID not generated');
        }
    });
};
function insertOrderId(req, res, orderid) {
    return new Promise(function (resolve, reject) {
        c.query('use ramcoeats', function (err) {
            if (err)
                reject(err);
            c.query('insert into cart (orderid,employeeid) values(:orderid,:employeeid)', { orderid: orderid, employeeid: req.body.employee_id }, function (err) {
                if (err)
                    reject(err);
                console.log('Inserted');
                resolve('Inserted');
            });
        });
    });
}
function deleteOrderId(req, res) {
    return new Promise(function (resolve, reject) {
        c.query('use ramcoeats', function (err) {
            if (err)
                reject(err);
            c.query('delete from cart where employeeid=:empid', { empid: req.body.employee_id }, function (err) {
                if (err)
                    reject(err);
                else {
                    resolve('Deleted');
                }
            });
        });
    });
}
function retriveOrderId(req, res) {
    return new Promise(function (resolve, reject) {
        c.query('use ramcoeats', function (err) {
            if (err)
                reject(err);
            c.query('select orderid from cart where employeeid=:empid', { empid: req.body.employee_id }, function (err, rows) {
                if (err)
                    reject(err);
                resolve(rows[0].orderid);
            });
        });
    });
}
exports.insertAndRetrieve = function (req, res, orderid) {
    return new Promise(function (resolve, reject) {
        c.query('use ramcoeats', function (err) {
            if (err)
                throw err;
            c.query('select status from orders  join cart  on cart.orderid=orders.order_id  where cart.employeeid=:empid', { empid: req.body.employee_id }, function (err, rows) {
                if (rows.length === 0) {
                    insertOrderId(req, res, orderid).then(function () { return retriveOrderId(req, res); }).then(function (orderId) { resolve(orderId); }).catch(err => reject(err));
                }
                else {
                    console.log("" + rows[0].status);
                    if (rows[0].status !== 'active') {
                        deleteOrderId(req, res).then(function () { return insertOrderId(req, res, orderid); }).then(function () { return retriveOrderId(req, res); }).then(function (orderId) { resolve(orderId); }).catch(err => reject(err));
                    }
                    else {
                        retriveOrderId(req, res).then(function (orderId) { resolve(orderId); }).catch(err => reject(err));
                    }
                }
            });
        });
    });
};
//# sourceMappingURL=order.utils.js.map