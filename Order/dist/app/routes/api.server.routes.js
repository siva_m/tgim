'use strict';
module.exports = function (app) {
    var orders = require('../../app/controllers/api.server.controller');
    app.route('/orders').get(orders.displayOrders);
    app.route('/orders/active').get(orders.displayActiveOrders);
    app.route('/orders').post(orders.insertOrder);
    app.route('/orders/:orderid').get(orders.displayOrder);
    app.route('/orders').put(orders.deleteOrder);
};
//# sourceMappingURL=api.server.routes.js.map